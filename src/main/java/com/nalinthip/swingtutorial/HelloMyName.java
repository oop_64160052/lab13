package com.nalinthip.swingtutorial;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class HelloMyName extends JFrame {
    JLabel lblName;
    JTextField txtName;
    JButton btnHello;
    JLabel lblHello;

    public HelloMyName() {
        super("Hello My Name");
        lblName = new JLabel("Name : ");
        lblName.setBounds(10, 10, 50, 20);
        lblName.setHorizontalAlignment(JLabel.RIGHT);

        txtName = new JTextField();
        txtName.setBounds(70, 10, 200, 20);

        btnHello = new JButton("Hello");
        btnHello.setBounds(30, 40, 250, 20);
        btnHello.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                String myName = txtName.getText();
                lblHello.setText("Hello " + myName);
            }

        });

        lblHello = new JLabel("Hello My Name");
        lblHello.setBounds(30, 70, 250, 20);
        lblHello.setHorizontalAlignment(JLabel.CENTER);

        this.add(lblName);
        this.add(txtName);
        this.add(btnHello);
        this.add(lblHello);

        this.setLayout(null);
        this.setSize(400, 300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    public static void main(String[] args) {
        HelloMyName frame = new HelloMyName();
    }
}
