package com.nalinthip.swingtutorial;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

public class ButtonExample1 extends JFrame {
    JButton button;
    JTextField text;
    JButton clearButton;

    public ButtonExample1() {
        super("Button Example");
        text = new JTextField();
        text.setBounds(50, 50, 150, 20);
        button = new JButton("Welcome");
        button.setBounds(50, 100, 110, 30);
        button.setIcon(new ImageIcon("welcome.png"));
        button.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                text.setText("Welcome to Burapha");
            }
        });
        clearButton = new JButton("Clear");
        clearButton.setBounds(175, 100, 95, 30);
        clearButton.setIcon(new ImageIcon("clear.png"));
        clearButton.addActionListener(new AbstractAction() {

            @Override
            public void actionPerformed(ActionEvent e) {
                text.setText("");
                
            }
            
        });

        this.add(clearButton);
        this.add(text);
        this.add(button);
        this.setSize(400, 400);
        this.setLayout(null);
        this.setVisible(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public static void main(String[] args) {
        ButtonExample1 frame = new ButtonExample1();
    }
}
